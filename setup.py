from setuptools import setup
from japong import name, get_version_string

with open("README", 'r') as f:
    long_description = f.read()

setup(
        name=name,
        version=get_version_string(),
        description="Tennis game based on kivy's 'pong' tutorial",
        license='',
        long_description=long_description,
        author='JA',
        author_email='',
        url='https://kivy.org/doc/stable/tutorials/pong.html',
        packages=['japong'],
        install_requires=[
            'Kivy',
        ],
        scripts=[]
)



