# -*- mode: python ; coding: utf-8 -*-

import os

os.environ['KIVY_NO_FILELOG'] = '1'
os.environ['KIVY_NO_CONSOLELOG'] = '1'
os.environ['KIVY_NO_ARGS'] = '1'

os.environ['KIVY_WINDOW'] = 'sdl2' # Values: sdl2, pygame, x11, egl_rpi
os.environ['KIVY_TEXT'] = 'sdl2' # Values: sdl2, pil, pygame, sdlttf
os.environ['KIVY_VIDEO'] = '' # Values: gstplayer, ffpyplayer, ffmpeg, null
os.environ['KIVY_AUDIO'] = '' # Values: sdl2, gstplayer, ffpyplayer, pygame, avplayer
os.environ['KIVY_IMAGE'] = 'sdl2' # Values: sdl2, pil, pygame, imageio, tex, dds, gif
os.environ['KIVY_CAMERA'] = '' # Values: avfoundation, android, opencv
os.environ['KIVY_SPELLING'] = '' # Values: enchant, osxappkit
os.environ['KIVY_CLIPBOARD'] = '' # Values: sdl2, pygame, dummy, android


from kivy_deps import sdl2, glew
from kivy.tools.packaging.pyinstaller_hooks import get_deps_minimal, get_deps_all, hookspath, runtime_hooks

deploy_path = os.path.realpath(os.curdir)
block_cipher = None
onefile = True
console = False
# for some unknown reason get_deps_minimal results in greater distribution size
# get_deps_all returns binaries, hidden imports and excludes
deps = get_deps_all()
binaries = deps["binaries"]
hiddenimports = deps["hiddenimports"]
excludes = deps["excludes"]
excludes += ["test", "unittest.test", "kivy.tools", "lib2to3", "win32com", "docutils"]
exckydes += ["sortedcontainers", "async-generator", "sniffio", "attrs", "outcome", "trio"]
exckydes += ["MarkupSafe", "Jinja2", "itsdangerous", "click", "Werkzeug", "flask"]
excludes += ["win32ui", "unicodedata", "ssl", "_queue", "overlapped", "_overlapped"]
excludes += ["_multiprocessing", "lzma", "hashlib", "_hashlib", "_elementtree", "_decimal", "asyncio", "bz2", "ctypes"]
excludes += ["win32api", "win32file", "win32gui", "win32wnet"]
# excludes += ["Mfc140u.dll"]
excludes += ["libcrypto-1_1.dll", "libtiff-5.dll", "libwebp-7.dll", "libFLAC-8.dll", "libmpg123-0.dll"]
excludes += ["libmpg123-0.dll", "libmodplug-1.dll", "libvorbis-0.dll", "libjpeg-9.dll", "libopus-0.dll"]
excludes += ["SDL2_mixer.dll", "VCRUNTIME140.dll", "libvorbisfile-3.dll", "libogg-0.dll", "libopusfile-0.dll"]

# excludes += "dbm,distutils,html,http,lib2to3,sqlite3,urllib,venv,wsgiref,xmlrpc".split(",")
# required: xml, email?

# manual changes::
hiddenimports.append("pkg_resources.py2_warn")

# set datas:
datas = [("archive\\README", "."),
         ("archive\\HISTORY", "."),
         ("archive\\data\\japong.png", ".\\data"),
         ("archive\\japong\\shapes.kv", ".\\japong")]

a = Analysis(['archive\\main.pyw'],
             pathex=[deploy_path],
             binaries=binaries,
             datas=datas,
             hiddenimports=hiddenimports,
             hookspath=hookspath(),
             runtime_hooks=runtime_hooks(),
             excludes=excludes,
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

if onefile:
    # ONE FILE MODE:
    exe = EXE(pyz,
              a.scripts,
              a.binaries,
              a.zipfiles,
              a.datas,
              *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
              [],
              name='japong',
              debug=False,
              bootloader_ignore_signals=False,
              strip=False,
              upx=True,
              upx_exclude=[],
              console=console,
              icon='archive\\japong.ico')

else:
    # ONE FOLDER MODE:
    exe = EXE(pyz,
              a.scripts,
              [],
              exclude_binaries=True,
              name='japong',
              debug=False,
              bootloader_ignore_signals=False,
              strip=False,
              upx=True,
              console=console,
              icon='archive\\japong.ico')

    coll = COLLECT(exe, #Tree('japong'),
                   a.binaries,
                   a.zipfiles,
                   a.datas,
                   *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
                   # *[Tree(p, prefix='lib\\sdl2') for p in sdl2.dep_bins],
                   # *[Tree(p, prefix='lib\\glew') for p in glew.dep_bins],
                   strip=False,
                   upx=True,
                   upx_exclude=[],
                   name='japong')

