#!/bin/bash

# Get directories
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPTS_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

BASE_DIR=${SCRIPTS_DIR%/*}
JAPONG_DIR=${BASE_DIR}/japong
DATA_DIR=${BASE_DIR}/data
DEPLOY_DIR=${BASE_DIR}/deploy/macos
BUILD_DIR=${DEPLOY_DIR}/build
DIST_DIR=${DEPLOY_DIR}/dist

# function to show message and set the error flag
HAYERROR=0
exitError () {
  cd $SCRIPTS_DIR || echo "can't find: $SCRIPTS_DIR"
  echo
  echo "... deployment cancelled :( ..."
  echo
  HAYERROR=1
}


echo ""
echo "---------------------------------------------"
echo "-   Japong Game - MacOS Deployment Script   -"
echo "---------------------------------------------"
echo ""

echo ... Enter admin pwd ...
sudo echo Let\'s Go... || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi

echo ""
echo ... Remove previous deployment ...
# check directory structure:
cd $BASE_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
cd $JAPONG_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
cd $DATA_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
cd $DEPLOY_DIR || mkdir -p $DEPLOY_DIR && cd $DEPLOY_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
rm -rf $BUILD_DIR
rm -rf $DIST_DIR
mkdir $BUILD_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
mkdir $BUILD_DIR/data || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
mkdir $DIST_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
cd $BUILD_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
cd $DIST_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi

echo ""
echo ... Prepare files to pack ...
cd $BASE_DIR
cp -r $JAPONG_DIR $BUILD_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
cp main.py $BUILD_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
cp README $BUILD_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
cp HISTORY $BUILD_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
cp $DATA_DIR/japong.ico $BUILD_DIR/data || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi

VERSION=$(kivy3 -c 'from japong import get_version_string;print(get_version_string())') || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
echo ""
echo ... Current Vesion = $VERSION...

echo ""
echo ... Compile Python Sources ...
# it is important to do this using kivy3, as it will be the same enviroment embedded in the app bundle
cd $BUILD_DIR
kivy3 -m nuitka --module japong --include-package=japong || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
# cp ./japong/shapes.kv .
rm -rf ./japong/*.py
rm -rf ./japong.build
rm -f japong.pyi

echo ""
echo ... Get kivy packager ...
cd $DEPLOY_DIR
sudo rm -rf kivy-sdk-packager || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
#git clone https://github.com/kivy/kivy-sdk-packager # this could mess up the version control in Pycharm project
#curl -O https://codeload.github.com/kivy/kivy-sdk-packager/zip/master && mv master kivy-sdk-packager-master.zip
unzip -quo $DATA_DIR/kivy-sdk-packager-master.zip && mv kivy-sdk-packager-master kivy-sdk-packager
cd kivy-sdk-packager/osx || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi

echo ""
echo ... Copy Kivy3.app ...
# exclusions could also be done passing a file to rsync, or later to package_app.py
rsync -a /Applications/Kivy3.app . \
                    --exclude='Contents/Frameworks/GS*.framework' \
                    --exclude='Contents/Frameworks/python/3.6.5/lib/python3.6/site-packages' \
                    --exclude='Contents/Frameworks/python/3.6.5/lib/python3.6/tkinter' \
                    --exclude='Contents/Frameworks/python/3.6.5/lib/python3.6/turtledemo' \
                    --exclude='Contents/Frameworks/python/3.6.5/lib/python3.6/unittest/test' \
                    --exclude='Contents/Frameworks/python/3.6.5/lib/python3.6/flask' \
                    --exclude='Contents/Frameworks/python/3.6.5/lib/python3.6/trio' \
                    --exclude='Contents/Resources/kivy/build' \
                    --exclude='Contents/Resources/kivy/doc' \
                    --exclude='Contents/Resources/kivy/examples' \
                    --exclude='Contents/Resources/kivy/kivy/test' \
                    --exclude='Contents/Resources/kivy/kivy/tools' || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi


echo ""
echo ... Clean up bundle ...
cd Kivy3.app || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
# copied from chreate.osx-bundle.sh
rm -rf "./Contents/Frameworks/python/3.6.5/lib/python3.6/share"
rm -rf "./Contents/Frameworks/python/3.6.5/lib/python3.6/"{sortedcontainers, async-generator, sniffio, attrs, outcome, trio}
rm -rf "./Contents/Frameworks/python/3.6.5/lib/python3.6/"{MarkupSafe,Jinja2,itsdangerous,click,Werkzeug,flask}
rm -rf "./Contents/Frameworks/python/3.6.5/lib/python3.6/"{test,unittest/test,turtledemo,tkinter}
rm -rf "./Contents/Frameworks/"{SDL2,SDL2_image,SDL2_ttf,SDL2_mixer,GStreamer}.framework/Headers
rm -rf "./Contents/Frameworks/"{SDL2,SDL2_image,SDL2_ttf,SDL2_mixer}.framework/Versions/A/Headers
rm -rf "./Contents/Frameworks/"SDL2_ttf.framework/Versions/A/Frameworks/FreeType.framework/Versions/A/Headers
rm -rf "./Contents/Frameworks/"SDL2_ttf.framework/Versions/A/Frameworks/FreeType.framework/Versions/Current
cd ./Contents/Frameworks/SDL2_ttf.framework/Versions/A/Frameworks/FreeType.framework/Versions || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
rm -rf Current
ln -s A Current
cd $DEPLOY_DIR/kivy-sdk-packager/osx/Kivy3.app/Contents/Frameworks || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
rm -rf "./SDL2_ttf.framework/Versions/A/Frameworks/FreeType.framework/Headers"
find -E . -regex '.*\.a$' -exec rm {} \;
find -E . -regex '.*\.la$' -exec rm {} \;
find -E . -regex '.*\.exe$' -exec rm {} \;

echo ""
echo ... Install required modules for packaging ...
pip3 install sh docopt

echo ""
echo ... Start packaging ...
cd $DEPLOY_DIR/kivy-sdk-packager/osx || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi

#header="#!python3"
#sed -i "1s/.*/'${header}'/" package_app.py
#sed -i '' "s:plistlib.readPlist:plistlib.load:g" package_app.py
#sed -i '' "s:plistlib.writePlist:plistlib.dump:g" package_app.py
sed -i '' "s:.decode('utf-8')::g" package_app.py
sed -i '' "s:/yourapp:/src:g" package_app.py
sed -i '' "s:/yourapp:/src:g" ./data/script
cp $DATA_DIR/japong.icns ./japong.icns || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
python3 package_app.py \
                  --author=Nautrania \
                  --appname=Japong \
                  --icon=japong.icns \
                  --source-app=Kivy3.app \
                  --displayname="Japong v$VERSION" \
                  --bundleid=com.nautrania.japong \
                  --bundlename=japong \
                  --bundleversion=$VERSION \
                  --strip=True \
                  --with-gstreamer=no \
                  $BUILD_DIR

echo ""
echo ...move app to distribution dir...
sed -i '' "s:/yourapp:/src:g" Japong.app/Contents/Resources/script
sed -i '' 's:org.kivy.osxlauncher:com.nautrania.japong:g' Japong.app/Contents/Info.plist
sed -i '' 's:Kivy:Japong:g' Japong.app/Contents/Info.plist
sed -i '' 's:kivy:japong:g' Japong.app/Contents/Info.plist
mv Japong.app/Contents/MacOS/Kivy Japong.app/Contents/MacOS/Japong
# remove frameworks not needed for this project
rm -rf Japong.app/Contents/Frameworks/SDL2_image.framework/Versions/A/Frameworks
rm -rf Japong.app/Contents/Frameworks/SDL2_mixer.framework/Versions/A/Frameworks
rm -f Japong.app/Contents/Frameworks/python/3.6.5/bin/python3.6m
rm -f Japong.app/Contents/Frameworks/python/3.6.5/bin/pyvenv-3.6
rm -f Japong.app/Contents/Frameworks/python/3.6.5/lib/python3.6/__pycache__/*
rm -f Japong.app/Contents/Frameworks/python/3.6.5/lib/python3.6/*/__pycache__/*
rm -f Japong.app/Contents/Frameworks/python/3.6.5/lib/python3.6/*/*/__pycache__/*
rm -rf Desktop/Japong.app/Contents/Frameworks/python/3.6.5/lib/python3.6/ctypes/test
rm -rf Japong.app/Contents/Frameworks/python/3.6.5/lib/python3.6/{dbm, distutils, \
            email, html, http, lib2to3, site-packages, sqlite3, urllib, venv, wsgiref, xml, xmlrpc}
rm -f Japong.app/Contents/Resources/kivy/kivy/__pycache__/*
rm -f Japong.app/Contents/Resources/kivy/kivy/*/__pycache__/*
rm -f Japong.app/Contents/Resources/kivy/kivy/*/*/__pycache__/*
rm -f Japong.app/Contents/Resources/kivy/kivy/*/*/*/__pycache__/*
rm -f Japong.app/Contents/Resources/kivy/kivy/data/fonts/DejaVuSans.ttf
rm -f Japong.app/Contents/Resources/kivy/kivy/*.{pyx,pxd,pxi}
rm -f Japong.app/Contents/Resources/kivy/kivy/*/*.{pyx,pxd,pxi}
rm -f Japong.app/Contents/Resources/kivy/kivy/*/*/*.{pyx,pxd,pxi}
rm -f Japong.app/Contents/Resources/kivy/kivy/*/*/*/*.{pyx,pxd,pxi}
rm -rf Japong.app/Contents/Resources/venv/lib/python3.6/site-packages/*
rm -rf Japong.app/Contents/Resources/get-pip.py
mv Japong.app $DIST_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi

echo ""
echo ...create disc image ...
cp -f $DATA_DIR/background.png ./data || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
cp -f $BUILD_DIR/README ./data/README.md || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
# patch script to exclude symlinkds scripts from the .dmg
# shellcheck disable=SC2016
line_to_replace='cp "data/${SYMLINKS_SCRIPT}" "${STAGING_DIR}/${SYMLINKS_SCRIPT}"'
# shellcheck disable=SC2016
line_replacement='cp "data/README.md" "${STAGING_DIR}/README.md"'
sed -i '' "s:${line_to_replace}:${line_replacement}:g" ./create-osx-dmg.sh
sed -i '' "s:window to {100, 100, 650, 501}:window to {100, 100, 650, 565}:g" ./create-osx-dmg.sh
sed -i '' "s:set icon size of viewOptions to 128:set icon size of viewOptions to 88:g" ./create-osx-dmg.sh
sed -i '' "s:window to {160, 265}:window to {155, 130}:g" ./create-osx-dmg.sh
sed -i '' "s:window to {384, 265}:window to {396, 130}:g" ./create-osx-dmg.sh
sed -i '' 's:close:set position of item "README.md" of container window to {280, 300} \
           close:g' ./create-osx-dmg.sh

#sed -i '' '/set background picture/d' ./create-osx-dmg.sh
cp -f $DATA_DIR/background.png ./data
sudo ./create-osx-dmg.sh $DIST_DIR/Japong.app
echo "read 'icns' (-16455) \"japong.icns\";" >> JapongIcon.rsrc
sudo Rez -a JapongIcon.rsrc -o Japong.dmg
sudo SetFile -a C Japong.dmg
sudo rm JapongIcon.rsrc
mv Japong.dmg $DIST_DIR/Japong-v$VERSION-macOS.dmg || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
#sudo ./create-osx-dmg.sh $DIST_DIR/JapongSrc.app
#mv JapongSrc.dmg $DIST_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi

echo ""
echo ... ALL\'S WELL ...
echo ""
cd $SCRIPTS_DIR || exitError && if [ "$HAYERROR" != "0" ]; then return 1; fi
