@echo off
@setlocal

REM https://kivy.org/doc/stable/guide/packaging-windows.html

echo .
echo -----------------------------------------------
echo -   Japong Game - Windows Deployment Script   -
echo -----------------------------------------------

echo .
echo - Verif dirs and remove previous deployment ...

set "SCRIPT_FILE=%~fp0"
REM echo SCRIPT_FILE = %SCRIPT_FILE%
call:set_prevdir SCRIPT_DIR %SCRIPT_FILE%
call:set_prevdir BASE_DIR %SCRIPT_DIR%

set JAPONG_DIR=%BASE_DIR%\japong
set DATA_DIR=%BASE_DIR%\data
set DEPLOY_DIR=%BASE_DIR%\deploy\win
set ARCHIVE_DIR=%DEPLOY_DIR%\archive
set BUILD_DIR=%DEPLOY_DIR%\build
set DIST_DIR=%DEPLOY_DIR%\dist

if not exist %DEPLOY_DIR% mkdir %DEPLOY_DIR%
if exist %ARCHIVE_DIR% rmdir /S /Q %ARCHIVE_DIR%
mkdir %ARCHIVE_DIR%
if exist %BUILD_DIR% rmdir /S /Q %BUILD_DIR%
REM mkdir %BUILD_DIR%
if exist %DIST_DIR% rmdir /S /Q %DIST_DIR%
REM mkdir %DIST_DIR%

REM echo SCRIPT_DIR='%SCRIPT_DIR%'
REM echo BASE_DIR='%BASE_DIR%'
REM echo JAPONG_DIR='%JAPONG_DIR%'
REM echo DATA_DIR='%DATA_DIR%'
REM echo DEPLOY_DIR='%DEPLOY_DIR%'
REM echo ARCHIVE_DIR='%ARCHIVE_DIR%'
REM echo BUILD_DIR='%BUILD_DIR%'
REM echo DIST_DIR='%DIST_DIR%'

echo .
echo - Copy files to build directory ...
cd %BASE_DIR%
xcopy /Q HISTORY %ARCHIVE_DIR%\.
xcopy /Q README %ARCHIVE_DIR%\.
xcopy /Q /I main.py %ARCHIVE_DIR%\.
REM changing extension to .pyw assumes windowed mode?
REM https://pyinstaller.readthedocs.io/en/stable/operating-mode.html#using-a-console-window
ren %ARCHIVE_DIR%\main.py main.pyw
mkdir %ARCHIVE_DIR%\japong
xcopy /Q japong %ARCHIVE_DIR%\japong
xcopy /Q data\japong.ico %ARCHIVE_DIR%\.
mkdir %ARCHIVE_DIR%\data
xcopy /Q data\japong.png %ARCHIVE_DIR%\data

echo .
echo - Compile Python sources
cd %ARCHIVE_DIR%
python -m nuitka --module japong --include-package=japong
del /Q /F japong\*.py
rmdir /Q /S japong.build
del /Q /F japong.pyi

REM goto:EXIT

echo .
echo - Pack with pyinstaller
cd %DEPLOY_DIR%
xcopy /F /Y %SCRIPT_DIR%\japong.spec .
::python -m PyInstaller --name japong --distpath %DIST_DIR% --icon %BUILD_DIR%\japong.ico %BUILD_DIR%\main.py
REM use japong.spec to configure packaging options
python -m PyInstaller japong.spec
REM the following is fot eh onefile package:
xcopy /Q /F /Y %ARCHIVE_DIR%\HISTORY %DIST_DIR%\.
xcopy /Q /F /Y %ARCHIVE_DIR%\README %DIST_DIR%\.

goto:EXIT

REM The folowing is for the onefolder package:
cd %DIST_DIR%\japong
rmdir /S /Q Include
del kivy_install\data\fonts\DejaVuSans.ttf
rmdir /S /Q kivy_install\modules\__pycache__
mkdir notice
move LICENSE.* notice\.
del /F /Q libcrypto-1_1.dll libtiff-5.dll libwebp-7.dll libFLAC-8.dll libmpg123-0.dll
del /F /Q libmpg123-0.dll libmodplug-1.dll libvorbis-0.dll libjpeg-9.dll libopus-0.dll
del /F /Q SDL2_mixer.dll VCRUNTIME140.dll libvorbisfile-3.dll libogg-0.dll libopusfile-0.dll


goto:EXIT

echo .
echo - Create installer with INNO setup

cd %DIST_DIR%
ISCC.exe %SCRIPT_DIR%\japong.iss


goto:EXIT

::--------------------------------------------------------
::-- Function section starts below here
::--------------------------------------------------------
:set_prevdir -- %1=VARIABLE_NAME, %2=PATH
SETLOCAL
set DIR=%2
REM echo DIR=%DIR%
:next_char
set CHAR=%DIR:~-1%
REM echo CHAR=%CHAR%
IF "%CHAR%"=="\" (
    ENDLOCAL
    set %1=%DIR:~0,-1%
) else (
    set DIR=%DIR:~0,-1%
    IF "%DIR%"=="" (
        echo ERROR, CAN'T FIND %1 FROM "%2"
    ) else (
        goto next_char
    )
)
goto:eof

:EXIT
