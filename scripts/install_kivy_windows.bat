
goto:EXIT

REM Ensure you have the latest pip, wheel, and virtualenv:
python -m pip install --upgrade pip wheel setuptools virtualenv

REM Install the dependencies (skip gstreamer (~120MB) if not needed, see Kivy’s dependencies)
python -m pip install docutils pygments pypiwin32 kivy_deps.sdl2==0.1.* kivy_deps.glew==0.1.*
python -m pip install kivy_deps.gstreamer==0.1.*

REM For Python 3.5+, you can also use the angle backend instead of glew. This can be installed with:
python -m pip install kivy_deps.angle==0.1.*

REM Install kivy:
python -m pip install kivy==1.11.1

REM  That did't work, I sued the following to install kivy with python 3.8:
wget https://download.lfd.uci.edu/pythonlibs/s2jqpv5t/Kivy-1.11.1-cp38-cp38-win_amd64.whl
python -m pip install Kivy-1.11.1-cp38-cp38-win_amd64.whl

REM (Optionally) Install the kivy examples:
python -m pip install kivy_examples==1.11.1


REM Note
REM If you encounter any permission denied errors,
REM try opening the Command prompt as administrator and trying again.
REM The best solution for this is to use a virtual environment instead.

:EXIT