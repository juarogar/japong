"""

"""

from kivy.properties import NumericProperty, StringProperty, ObjectProperty

from japong.collision import *
from japong.ball import Ball


class Racket(Widget):
    app: ObjectProperty(None)
    ball: Ball = ObjectProperty(None)
    games = NumericProperty(0.)
    score = NumericProperty(0.)
    score_str = StringProperty("  0 | 00")
    player_name = StringProperty("Player")
    min_x = NumericProperty(0.)
    max_x = NumericProperty(0.)
    min_y = NumericProperty(0.)
    max_y = NumericProperty(0.)
    side = NumericProperty(0.)
    speed = NumericProperty(0)

    def __init__(self, **kwargs):
        super(Racket, self).__init__(**kwargs)
        self.free = True
        self._speed: Vector = Vector(0, 0)
        self._accel: Vector = Vector(0, 0)
        self._steps: Vector = Vector(0, 0)

    def __str__(self):
        return f"Player {self.player_name}"

    def set_position(self, x, y):
        # check if we cross the limits of the court before setting the a new position:
        center_x = x + 0.5 * self.width
        if center_x < self.min_x:
            self.center_x = self.min_x
            self._speed.x = 0
            self._accel.x = 0
        elif center_x > self.max_x:
            self.center_x = self.max_x
            self._speed.x = 0
            self._accel.x = 0
        else:
            self.center_x = center_x
        center_y = y + 0.5 * self.height
        if center_y < self.min_y:
            self.center_y = self.min_y
            self._speed.y = 0
            self._accel.y = 0
        elif center_y > self.max_y:
            self.center_y = self.max_y
            self._speed.y = 0
            self._accel.y = 0
        else:
            self.center_y = center_y

    def set_speed(self, speed_x, speed_y):
        speed = Vector(speed_x, speed_y)
        n = speed.length()
        if n > PLAYER_MAX_SPEED:
            self._speed = speed * PLAYER_MAX_SPEED / n
            self.speed = PLAYER_MAX_SPEED
        elif n < ERR:
            self._speed = Vector(0., 0.)
            self.speed = 0.
        else:
            self._speed = speed
            self.speed = n

    def accelerate(self, accel_increase_x: float, accel_increase_y: float):
        accel = Vector(self._accel.x + accel_increase_x, self._accel.y + accel_increase_y)
        n = accel.length()
        if n > PLAYER_MAX_ACCELERATION:
            self._accel = accel * PLAYER_MAX_ACCELERATION / n
        elif n < ERR:
            self._accel = Vector(0., 0.)
        else:
            self._accel = accel

    def move(self):
        if self.free: # KEYBOARD MODE
            if self.speed < PLAYER_SPEED_DECAY: # calculate friction with previous speed
                fzt = -self._speed
            else:
                fzt = -self._speed * PLAYER_SPEED_DECAY / self.speed
            speed = self._speed + fzt + self._accel / CYCLES_PER_SECOND # calculate speed with previous acceleration
            a = self._accel.length()
            if a < PLAYER_ACCELERATION_DECAY: # update acceleration
                self._accel = Vector(0, 0)
            else:
                self._accel -= self._accel * PLAYER_ACCELERATION_DECAY / a
            vmove = (self._steps + speed / CYCLES_PER_SECOND) * self.app.ft # calculate distance with new speed
            speed += self._steps / PLAYER_IMPULSE # add step impulse to speed
        else: # POINTER MODE
            self._accel = Vector(0., 0.)
            vmove = self._steps * self.app.ft # distance in pixels
            speed = MOUSE_SPEED_ADJUSTMENT * self._steps * CYCLES_PER_SECOND # speed in ft / seconds

        self.set_speed(speed.x, speed.y)
        self._steps = Vector(0, 0) # remove previous steps if any
        move = vmove.length()
        if self.ball.momentum < ERR < move: # move the racket if not touching the ball
            col_move = -ball_racket_collision_move(self.ball, -vmove, self)
            self.set_position(self.x + col_move.x, self.y + col_move.y)
            if self.center_x in [self.min_x, self.max_x] or self.center_y in [self.min_y, self.max_y]:
                # if the racket touches a limit, no movement left, set move to 1 in the event there also is a collision
                move = 0 if not -ERR < ball_racket_distance(self.ball, self) < ERR else 1
            else:
                # if not vequal(vmove, col_move):
                #     if not -ERR < ball_racket_distance(self.ball, self) < ERR:
                #         print(f"ERROR: {self}.move: racket under/over aproach")
                vmove -= col_move
                move = vmove.length()
        #     if ball_racket_distance(self.ball, self) < -ERR:
        #         print(f"ERROR: {self}.move: racket run over")
        # if ball_racket_distance(self.ball, self) < -1:
        #     print(f"ERROR: {self}.move: ball run over, overlap={-ball_racket_distance(self.ball, self)}")

        if move > ERR or self.ball.momentum > ERR: # hit
            # if not -ERR < ball_racket_distance(self.ball, self) < ERR:
            #     print(f"ERROR: {self}.move: under/over aproach")
            self.ball.time_to_hit = 0
            self._accel = Vector(0., 0.) # cancel acceleration if there is a hit

            ball_speed = self.ball.speed_vector
            hit_side_x = 1 if self.ball.center_x > self.center_x else -1
            hit_side_y = 1 if self.ball.center_y > self.center_y else -1

            cx, cy, cr = self.ball.center_x, self.ball.center_y, 0.5 * self.ball.width
            dx_center = point_segment_distance(cx, self.x, self.right)
            dy_center = point_segment_distance(cy, self.y, self.top)
            spin = 0
            hit_angle = 0
            hit_extra = Vector(0, 0)
            hit_type = ""

            adx, ady = abs(dx_center), abs(dy_center)
            if adx < ERR and ady < ERR:
                # hit_type = "IMPOSSIBLE"
                # print("ERROR: unexpected overlap")
                hit_angle = Vector(hit_side_x, hit_side_y).angle(Vector(1, 0))
                hit_extra.x += 0.5 * HIT_SWING_SPEED
            elif adx < ERR: # SIDE HIT
                # hit_type = "SIDE"
                hit_angle = 90 * hit_side_y
            elif ady < ERR: # FULL HIT
                # hit_type = "FULLL"
                hit_angle = 90 * (1 - hit_side_x)
                tack = 2 * (self.ball.center_y - self.center_y) / self.height  # tack is between [-1, 1]
                spin = hit_side_x * tack * BALL_MAX_SPIN / CYCLES_PER_SECOND
                if tack * ball_speed.y > 0:
                    hit_angle += -HIT_MAX_ANGLE * tack * hit_side_x
                    hit_extra.x += HIT_SWING_SPEED
                else:
                    hit_extra.x += HIT_SWING_SPEED
                    # ra = math.radians(-hit_side_x * tack * HIT_MAX_ANGLE * (1 - hit_side_y * speed.y / PLAYER_MAX_SPEED))
                    # hit_extra.x += HIT_SWING_SPEED * math.cos(ra)
                    # hit_extra.y += HIT_SWING_SPEED * math.sin(ra)
            else: # NEAR MISS
                # hit_type = "NEAR MISS"
                hit_angle = 90 * (1 - hit_side_x) + math.degrees(math.atan(dy_center / dx_center))
                tack = ball_speed.angle(Vector(-1, 0).rotate(hit_angle)) / 90
                spin = tack * BALL_MAX_NEAR_MISS_SPIN / CYCLES_PER_SECOND

            hit = speed.rotate(-hit_angle)
            t_bs = ball_speed.rotate(-hit_angle)

            # if t_bs.x * hit.x > ERR:
            #     hit_type += " PUSH" if t_bs.x > 0 else " HOLD"
            if ERR > t_bs.x > hit.x + hit_extra.x: # HOLD HIT correction
                # print("error: HOLD HIT, wrong speed, racket is moving faster than the ball?")
                hit_extra.x = 0.9 * t_bs.x - hit.x
            elif ERR < hit.x < t_bs.x + hit_extra.x: # PUSH HIT correction
                # this can happen because of key steps not adding enough speed to the racket
                hit_extra.x = 1.1 * t_bs.x - hit.x

            # player mass adjustment depending on the distance to the net,
            # this will give more power to hits from the back, and slow down ball when hit close to the net
            # it will also produce greater step back of the player away from the net
            dist_to_net = (self.max_x - self.x) if self.side > 0 else (self.x - self.min_x)
            pmass = PLAYER_MASS * (0.1 + dist_to_net / (self.max_x - self.min_x))
            m_tot, m_diff = BALL_MASS + pmass, pmass - BALL_MASS

            impact_fact = 3
            t_next_bs = Vector((-t_bs.x * m_diff + 2. * (hit.x + hit_extra.x) * pmass) / m_tot, t_bs.y + hit_extra.y)
            t_next_rs = Vector((hit.x * m_diff + 2 * impact_fact * t_bs.x * BALL_MASS) / m_tot, hit.y)
            next_ball_speed = t_next_bs.rotate(hit_angle)
            next_speed = t_next_rs.rotate(hit_angle)

            self.ball.set_spin(spin)
            self.ball.set_speed(next_ball_speed.x, next_ball_speed.y)
            self.set_speed(next_speed.x, next_speed.y)
            # if not -ERR < ball_racket_distance(self.ball, self) < ERR:
            #     print(f"ERROR {self}.move({hit_type}): unexpected collision distance")
            self.ball.move_away(0.6 * self.app.ft * self.ball.speed / CYCLES_PER_SECOND, self)
            # if ball_racket_distance(self.ball, self) < ERR:
            #     print(f"ERROR: {self}.move({hit_type}): ball momentum collision")
            vmove = 0.5 * self.app.ft * self._speed / CYCLES_PER_SECOND
            if vmove.length() > ERR:
                col_move = -ball_racket_collision_move(self.ball, -vmove, self)
                self.set_position(self.x + col_move.x, self.y + col_move.y)
            #     if ball_racket_distance(self.ball, self) < -ERR:
            #         print(f"ERROR: {self}.move({hit_type}): racket momentum run over")
            #     elif ball_racket_distance(self.ball, self) < ERR:
            #         print(f"WARNING: {self}.move({hit_type}): momentum collision")
            # if ball_racket_distance(self.ball, self) < ERR:
            #     print(f"ERROR {self}.move({hit_type}): double hit")

        # if ball_racket_distance(self.ball, self) < ERR:
        #     print(f"ERROR {self}.move: end of movement collision") # this should not happen
        #     if self.ball.speed_x > 0:
        #         px = max(self.ball.x, self.right + 1)
        #     else:
        #         px = min(self.ball.x, self.x - self.ball.width - 1)
        #     # if self.ball.speed_y > 0:
        #     #     py = max(self.ball.y, self.top + 1)
        #     # else:
        #     #     py = min(self.ball.y, self.y - self.ball.height - 1)
        #     self.ball.set_position(px, self.ball.y)
        #     if ball_racket_distance(self.ball, self) < ERR:
        #         print(f"ERROR {self}.move(): can't move ball away")
        
    ####################################
    # KEYBOARD MOVEMENT                #
    ####################################
    def key_up(self, slow=False):
        if self._speed.y >= -ERR or slow:
            self.accelerate(0, PLAYER_MIN_ACCELERATION)
        else:
            self.accelerate(0, min(-self._accel.y, PLAYER_BRAKES))
        self._steps += Vector(0, PLAYER_SIDE_STEP * (1 if not slow else 0.3))

    def key_down(self, slow=False):
        if self._speed.y <= ERR or slow:
            self.accelerate(0, -PLAYER_MIN_ACCELERATION)
        else:
            self.accelerate(0, max(-self._accel.y, -PLAYER_BRAKES))
        self._steps += Vector(0, -PLAYER_SIDE_STEP * (1 if not slow else 0.3))

    def key_left(self):
        if self._speed.x <= ERR:
            self.accelerate(-PLAYER_MIN_ACCELERATION, 0)
            self._steps += Vector(-PLAYER_FRONT_STEP, 0)
        else:
            self.accelerate(max(-self._accel.x, -PLAYER_BRAKES), 0)
            self._steps += Vector(-PLAYER_BACK_STEP, 0)

    def key_right(self):
        if self._speed.x >= -ERR:
            self.accelerate(PLAYER_MIN_ACCELERATION, 0)
            self._steps += Vector(PLAYER_FRONT_STEP, 0)
        else:
            self.accelerate(min(-self._accel.x, PLAYER_BRAKES), 0)
            self._steps += Vector(PLAYER_BACK_STEP, 0)

    ####################################
    # MOUSE MOVEMENT                   #
    ####################################
    def move_to_touch(self, touch):
        dx = max(self.min_x, min(self.max_x, touch.x)) - self.center_x
        dy = max(self.min_y, min(self.max_y, touch.y)) - self.center_y
        # convert distance to steps (in foot)
        self._steps = Vector(dx, dy) / self.app.ft

    ####################################
    # SCORE MANAGEMENT                 #
    ####################################

    def add_game(self):
        self.games += 1
        self.reset_score()

    def add_point(self):
        self.score += 1
        self.update_score()

    def remove_point(self):
        self.score -= 1
        self.update_score()

    def reset_score(self):
        self.score = 0
        self.update_score()

    def update_score(self):
        if self.score == 0:
            score = "00"
        elif self.score == 1:
            score = "15"
        elif self.score == 2:
            score = "30"
        elif self.score == 3:
            score = "40"
        elif self.score == 4:
            score = "AD"
        else:
            score = "??"

        self.score_str = f" {self.games:>2.0f} | {score}"
