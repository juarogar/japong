"""

"""
import random
import datetime

from kivy.properties import ObjectProperty, NumericProperty

from japong.collision import *


class Ball(Widget):
    app: ObjectProperty(None)
    speed = NumericProperty(0)
    angle = NumericProperty(0)

    def __init__(self, **kwargs):
        super(Ball, self).__init__(**kwargs)
        self._speed: Vector = Vector(0, 0)
        self._spin: float = 0.
        self.time_to_hit = 0.
        self.momentum: float = 0.
        self.prev_pos = Vector(0, 0)

    def __str__(self):
        return "The Ball"

    def set_position(self, x, y):
        if x != self.x or y != self.y:
            self.prev_pos = Vector(self.x, self.y)
        self.x = x
        self.y = y

    @property
    def speed_vector(self) -> Vector:
        return self._speed

    @property
    def speed_x(self) -> float:
        return self._speed[0]

    @property
    def speed_y(self) -> float:
        return self._speed[1]

    def stop(self):
        self._speed = Vector(0., 0.)
        self.speed = 0
        self._spin = 0
        self.time_to_hit = 0

    def set_speed(self, speed_x, speed_y):
        speed = Vector(speed_x, speed_y)
        if abs(speed.x) < 1:
            speed.x = random.randrange(-1, 2, 2)
        r = speed.length() # round(speed.length(), DECIMALS)
        if r > BALL_MAX_SPEED:
            self._speed = speed * BALL_MAX_SPEED / r
            self.speed = BALL_MAX_SPEED
        elif r < BALL_MIN_SPEED:
            self._speed = speed * BALL_MIN_SPEED / r
            self.speed = BALL_MIN_SPEED
        else:
            self._speed = speed
            self.speed = r
        self.angle = (180 / math.pi) * math.atan2(speed_y, speed_x)

    def rotate(self, angle):
        self._speed = self._speed.rotate(self._spin)
        self.angle += angle

    def move_away(self, minimum, racket):
        m = max(self.momentum, minimum)
        self.momentum = 0
        if self.speed == 0 or m == 0:
            return
        vmove = m * self._speed / self.speed
        col_move = ball_racket_collision_move(self, vmove, racket)
        # if ball_racket_distance(self, racket) < -ERR:
        #     print(f"ERROR ball.move_away: overlap")
        self.set_position(self.x + col_move.x, self.y + col_move.y)
        # if ball_racket_distance(self, racket) < ERR:
        #     print(f"ERROR ball.move_away: unexpected collision")

    @property
    def spin(self) -> float:
        return self._spin

    def set_spin(self, spin):
        if spin > BALL_MAX_SPIN:
            self._spin = BALL_MAX_SPIN
        elif spin < -BALL_MAX_SPIN:
            self._spin = -BALL_MAX_SPIN
        else:
            self._spin = spin

    def move(self, *obstacles):
        # calculate friction deceleration * time (speed decrease) and new speed:
        if self.speed == 0:
            return
        if self.speed > BALL_SPEED_DECAY + BALL_MIN_SPEED:
            self._speed *= (1 - BALL_SPEED_DECAY / self.speed)
            self.speed -= BALL_SPEED_DECAY
        elif self.speed > BALL_SPEED_DECAY:
            self._speed *= BALL_MIN_SPEED / self.speed
            self.speed = BALL_MIN_SPEED
        self.rotate(self._spin)
        # calculate movement to new position, but don't go over obstacles
        vmove = self._speed * self.app.ft / CYCLES_PER_SECOND
        ts = datetime.datetime.now().timestamp()
        if ts >= self.time_to_hit:
            delta = 2.
            move = vmove.length()
            # cx, cy, cr = self.center_x, self.center_y, 0.5 * self.width
            min_distance = (delta * (self.speed + PLAYER_MAX_SPEED) + 2 * PLAYER_SIDE_STEP) * self.app.ft
            for ob in obstacles:
                distance_to_hit = ball_racket_distance(self, ob)
                if distance_to_hit > min_distance:
                    continue
                # elif distance_to_hit < ERR:
                #     print("ERROR ball.move: blocked")
                #     return # unexpected collision? this should not happen here
                elif distance_to_hit <= move:
                    col_move = ball_racket_collision_move(self, vmove, ob)
                    if not vequal(col_move, vmove):
                        self.momentum = (vmove - col_move).length()
                        self.set_position(self.x + col_move.x, self.y + col_move.y)
                        # if not -ERR < ball_racket_distance(self, ob) < ERR:
                        #     print("ERROR ball.move: over/under hit")
                        return # ball hits the racket first
                time_to_hit = (distance_to_hit / self.app.ft - 2 * PLAYER_SIDE_STEP) / (self.speed + PLAYER_MAX_SPEED)
                delta = min(delta, time_to_hit)
            self.time_to_hit = ts + delta
        self.set_position(self.x + vmove.x, self.y + vmove.y)
        # br1 = ball_racket_distance(self, obstacles[0])
        # if br1 < ERR:
        #     print("ERROR ball.move: blind hit")
        # br2 = ball_racket_distance(self, obstacles[1])
        # if br2 < ERR:
        #     print("ERROR ball.move: blind hit")
        # delta = 0
