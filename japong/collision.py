"""

"""
import math

from kivy.uix.widget import Widget
from kivy.vector import Vector

from japong.config import *


def vround(v: Vector) -> Vector:
    return Vector(round(v.x, DECIMALS), round(v.y, DECIMALS))


def vequal(v1: Vector, v2: Vector) -> bool:
    # when comparing vectors, precision error may produce false inequalities, we need to round first.
    return vround(v1) == vround(v2)


# avoid kivy implementation of Vector.normalize(), we've seen division by 0 exception due to rounding errors
def vnorm(v: Vector) -> Vector:
    n = v.length()
    return Vector(0, 0) if abs(n) <= ERR else v / n


def point_segment_distance(a, b1, b2) -> float:
    if a < b1:
        return b1 - a # >0 if b1.b2 is to the right of a
    elif a > b2:
        return b2 - a # <0 if b1.b2 is to the left of a
    return 0. # =0 if a is between or at b1 and b2


def segments_distance(a1, a2, b1, b2) -> float:
    if a1 > b2:
        return b2 - a1  # >0 if b is to the right of a
    elif a2 < b1:
        return b1 - a2  # <0 if b is to the left of a
    return 0.  # =0 if segments touch or overlap


def _rect_circle_distance(x, y, xx, yy, cx, cy, cr):
    dx_center = point_segment_distance(cx, x, xx)
    dy_center = point_segment_distance(cy, y, yy)
    d_center = math.sqrt(dx_center ** 2 + dy_center ** 2)
    return d_center - cr # > 0 if not touching, < 0 if overlap, 0 if circle completly inside


def ball_racket_collision(ball: Widget, racket: Widget):
    cr = round(0.5 * ball.width, DECIMALS)
    dx_center = point_segment_distance(ball.center_x, racket.x, racket.right)
    if abs(dx_center) > cr:
        return False
    dy_center = point_segment_distance(ball.center_y, racket.y, racket.top)
    if abs(dy_center) > cr:
        return False
    d2_center = round(dx_center ** 2 + dy_center ** 2, DECIMALS)
    return d2_center <= cr ** 2


def ball_racket_distance(ball: Widget, racket: Widget):
    # 0 if touching, > 0 if overlap, < 0 if no collision
    x, y, xx, yy = racket.x, racket.y, racket.right, racket.top
    cx, cy, cr = ball.center_x, ball.center_y, 0.5 * ball.width
    return _rect_circle_distance(x, y, xx, yy, cx, cy, cr)


def ball_racket_collision_move(ball: Widget, vmove: Vector, racket: Widget) -> Vector:
    cx, cy, cr = ball.center_x, ball.center_y, 0.5 * ball.width
    x, y, xx, yy = racket.x, racket.y, racket.right, racket.top

    dx_center = point_segment_distance(cx, x, xx)
    if 0 < vmove.x <= dx_center - cr or dx_center + cr <= vmove.x < 0:
        return vmove

    dy_center = point_segment_distance(cy, y, yy)
    if 0 < vmove.y <= dy_center - cr or dy_center + cr <= vmove.y < 0:
        return vmove

    # Test if the movement takes the ball away from the racket
    if -90 <= vmove.angle(Vector(-dx_center, -dy_center)) <= 90:
        return vmove

    distance = math.sqrt(dx_center ** 2 + dy_center ** 2) - cr
    if distance < ERR:
        # Ball and racket are touching or overlapping, can't move if direction is not bringing the ball away
        return Vector(0., 0.)

    move = vmove.length() # the movement is shorter than the current distance between ball and racket
    if distance - move > ERR:
        return vmove

    # calculate "safe" movement from the circle to touch one side of the racket
    amx = abs(vmove.x) if dx_center * vmove.x > ERR else 0
    amy = abs(vmove.y) if dy_center * vmove.y > ERR else 0
    mx = (abs(dx_center) - cr) / amx if amx > ERR else 0
    my = (abs(dy_center) - cr) / amy if amy > ERR else 0
    svmove = vmove * max(mx, my)
    scx, scy = Vector(cx, cy) + svmove
    sdx_center = point_segment_distance(scx, x, xx)
    sdy_center = point_segment_distance(scy, y, yy)
    sdist_center = math.sqrt(sdx_center ** 2 + sdy_center ** 2)
    sdistance = sdist_center - cr
    if round(sdistance, DECIMALS) == 0:
        return svmove # can only move until touching the rectangle on one side

    remaing_move = vmove - svmove
    if 0 < remaing_move.x <= sdx_center - cr or sdx_center + cr <= remaing_move.x < 0:
        return vmove
    if 0 < remaing_move.y <= sdy_center - cr or sdy_center + cr <= remaing_move.y < 0:
        return vmove

    # if we are here, the collision could only happen at the corner
    # then, svdist_center goes from the circle centre to the corner of the rectangle
    # calculate min distance vector (vmd) from the circle path to the rectangle:
    svdist_center = Vector(sdx_center, sdy_center)
    sangle = svdist_center.angle(vmove)
    vmove_to_tangent = vmove * math.cos(math.radians(sangle)) * sdist_center / move
    vmd = svdist_center - vmove_to_tangent
    md = vmd.length()
    if md > cr: # if the min distance is greater than the radius, there is no collision along the path
        return vmove
    # calculate circle touching point vector vtp, then return the exact collision movement
    vcol_move = svmove + vmove_to_tangent - math.sqrt(cr ** 2 - md ** 2) * vmove / move
    cmove = vcol_move.length()
    if cmove > move:
        return vmove
    else:
        return vcol_move
