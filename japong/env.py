"""
 https://kivy.org/doc/stable/guide/environment.html#restrict-core-to-specific-implementation
"""
import os

os.environ['KIVY_NO_FILELOG'] = '1'
# os.environ['KIVY_NO_CONSOLELOG'] = '1'
os.environ['KIVY_NO_ARGS'] = '1'

os.environ['KIVY_WINDOW'] = 'sdl2' # Values: sdl2, pygame, x11, egl_rpi
os.environ['KIVY_TEXT'] = 'sdl2' # Values: sdl2, pil, pygame, sdlttf
os.environ['KIVY_VIDEO'] = '' # Values: gstplayer, ffpyplayer, ffmpeg, null
os.environ['KIVY_AUDIO'] = '' # Values: sdl2, gstplayer, ffpyplayer, pygame, avplayer
os.environ['KIVY_IMAGE'] = 'sdl2' # Values: sdl2, pil, pygame, imageio, tex, dds, gif
os.environ['KIVY_CAMERA'] = '' # Values: avfoundation, android, opencv
os.environ['KIVY_SPELLING'] = '' # Values: enchant, osxappkit
os.environ['KIVY_CLIPBOARD'] = '' # Values: sdl2, pygame, dummy, android

