"""

"""
import os
from random import randint

from kivy.config import Config
from kivy.app import App
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.properties import NumericProperty

from japong.config import *
from japong.court import Court
from japong.shapes import shapes


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    # This solves an issue when building a one file exe in windows with PyInstaller
    # https://irwinkwan.com/2013/04/29/python-executables-pyinstaller-and-a-48-hour-game-design-compo/
    # https://stackoverflow.com/questions/7674790/bundling-data-files-with-pyinstaller-onefile
    # https://stackoverflow.com/questions/35952595/kivy-compiling-to-a-single-executable
    import sys
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)


class JapongApp(App):
    ft = NumericProperty(0)
    adj_x = NumericProperty(0)
    adj_y = NumericProperty(0)

    def __init__(self, **kwargs):
        Config.set('input', 'mouse', 'mouse,multitouch_on_demand')
        try:
            icon_file = os.path.join('data', 'japong.png')
            if os.path.exists(resource_path(icon_file)):
                Window.set_icon(resource_path(icon_file))
            elif os.path.exists(icon_file):
                Window.set_icon(icon_file)
        finally:
            pass

        Window.show_cursor = False
        Window.set_system_cursor("hand")
        Window.fullscreen = FULL_SCREEN
        self.set_scale()
        Window.bind(on_resize=self.set_scale)
        self._clock_event = None

        super(JapongApp, self).__init__(**kwargs)

        try:
            shapes_file = os.path.join('japong', 'shapes.kv')
            if os.path.exists(shapes_file):
                Builder.load_file(os.path.join('japong', 'shapes.kv'))
                # we load the file first, don't save it if there are errors
                with open(os.path.join('japong', 'shapes.kv'), "r") as fkv:
                    with open(os.path.join('japong', 'shapes.py'), "w") as fpy:
                        content = fkv.read().replace('\\', '\\\\')
                        fpy.write(f'shapes = """\n{content}\n"""')
                return
            elif os.path.exists(resource_path(shapes_file)):
                Builder.load_file(resource_path(shapes_file))
                return
        except: # when double-click the .app file in mac, the previous code causes an exception?
            pass

        Builder.load_string(shapes)

    def set_scale(self, *args):
        self.ft = round(min(Window.width / 120, Window.height / 60), 12)
        self.adj_x = round(0.5 * (Window.width / self.ft - 120), 11)
        self.adj_y = round(0.5 * (Window.height / self.ft - 60), 11)

    def build(self):
        court = Court()
        Clock.schedule_once(self.delayed_start, 1.0)
        return court

    def delayed_start(self, *args):
        self.start_clock()
        Clock.schedule_once(lambda dt: self.root.serve_ball((2 * randint(0, 1) - 1) * BALL_INIT_SPEED), 1.0)

    def start_clock(self, *args):
        self.stop_clock()
        self._clock_event = Clock.schedule_interval(self.root.update, CYCLE / 1000.)

    def stop_clock(self, *args):
        if self._clock_event is not None:
            self._clock_event.cancel()
            self._clock_event = None
