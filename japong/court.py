"""

"""
from kivy.core.window import Window
from kivy.properties import ObjectProperty, NumericProperty
from kivy.clock import Clock
from kivy.uix.popup import Popup
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.button import Button

from japong.collision import *
from japong.ball import Ball
from japong.racket import Racket


class Court(Widget):
    app: ObjectProperty(None)
    ball: Ball = ObjectProperty(None)
    player1: Racket = ObjectProperty(None)
    player2: Racket = ObjectProperty(None)
    effective_cycle = NumericProperty(0)

    def __init__(self, **kwargs):
        super(Court, self).__init__(**kwargs)
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self.mouse_counter = 0
        self.mouse_pos = Window.mouse_pos
        self.player1.free = True
        self.player2.free = True
        self.paused = False
        self.popup = None
        self.dt = 0.
        self.cycle_count = 0

    def serve_ball(self, ball_speed):
        self.ball.stop()
        if self.player1.score >= 3 and self.player1.score - self.player2.score >= 2:
            self.player1.add_game()
            self.player2.reset_score()
        elif self.player2.score >= 3 and self.player2.score - self.player1.score >= 2:
            self.player2.add_game()
            self.player1.reset_score()
        self.ball.center_x = self.center_x
        self.ball.center_y = (self.player1.center_y if ball_speed < 0 else self.player2.center_y)
        self.ball.time_to_hit = 0
        Clock.schedule_once(lambda dt: self.ball.set_speed(ball_speed, 0), 2.0)
        # self.app.start_clock()

    def pause_game(self, msg=f"waiting..."):
        if self.paused and self.popup is not None:
            self.popup.content.text = msg
        else:
            self.app.stop_clock()
            self.paused = True
            Window.show_cursor = True
            lbl = Label(text=msg)
            btn = Button(text="Click here or \npress 'p' to continue")
            btn.bind(on_press=self.continue_game)
            box = BoxLayout(orientation='vertical', spacing=0.5 * self.app.ft)
            box.add_widget(lbl)
            box.add_widget(btn)  # adds to 0 index, so add from bottom to top
            popup = Popup(title='GAME PAUSED',
                          content=box,
                          size_hint=(0.40, 0.30),
                          pos_hint={'center_x': 0.5, 'center_y': 0.5},
                          auto_dismiss=False)
            popup.open()
            self.popup = popup

    def continue_game(self, *args):
        Window.show_cursor = False
        self.popup.dismiss()
        self.popup = None
        self.paused = False
        self.app.start_clock()

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] == 'p':
            if self.popup is None:
                self.pause_game("Paused by user")
            else:
                self.continue_game()
        elif keycode[1] == 'f':
            Window.fullscreen = False if Window.fullscreen == 'auto' else 'auto'
        elif keycode[1] == '2':
            self.player1.key_up(True)
        elif keycode[1] == 'e':
            self.player1.key_down(True)
        elif keycode[1] == 'w':
            self.player1.key_up()
        elif keycode[1] == 's':
            self.player1.key_down()
        elif keycode[1] == 'd':
            self.player1.key_right()
        elif keycode[1] == 'a':
            self.player1.key_left()
        elif keycode[1] == 'rshift':
            self.player2.key_up(True)
        elif keycode[1] == '-':
            self.player2.key_down(True)
        elif keycode[1] == 'up':
            self.player2.key_up()
        elif keycode[1] == 'down':
            self.player2.key_down()
        elif keycode[1] == 'right':
            self.player2.key_right()
        elif keycode[1] == 'left':
            self.player2.key_left()

    def update(self, dt):
        self.cycle_count += 1
        self.dt += dt
        if self.cycle_count >= CYCLES_PER_SECOND:
            self.effective_cycle = round(1000 * self.dt / self.cycle_count)
            self.dt = 0
            self.cycle_count = 0

        if self.paused: # is this necessary?
            return

        self.ball.move(self.player1, self.player2)

        # move player hitting the ball first, choose the one closer to the ball
        if self.ball.center_x < self.center_x:
            self.player1.move()
            self.player2.move()
        else:
            self.player2.move()
            self.player1.move()

        # went of to a side to score point?
        if self.ball.center_x < self.x + self.app.adj_x * self.app.ft + ERR:
            if self.player1.score == 4:
                self.player1.remove_point() # deuce
            else:
                self.player2.add_point()
            self.serve_ball(ball_speed=BALL_INIT_SPEED)
            return
        if self.ball.center_x > self.width - self.app.adj_x * self.app.ft - ERR:
            if self.player2.score == 4:
                self.player2.remove_point() # deuce
            else:
                self.player1.add_point()
            self.serve_ball(ball_speed=-BALL_INIT_SPEED)
            return

        # bounce ball off bottom or top
        if self.ball.y <= self.y + self.app.adj_y * self.app.ft - ERR:
            self.ball.y = self.y + self.app.adj_y * self.app.ft
            self.ball.set_speed(self.ball.speed_x, max(BALL_MIN_SPEED, -self.ball.speed_y))
            self.ball.set_spin(0)
            if ball_racket_distance(self.ball, self.player1) < ERR:
                print("ERROR: court.update: ball bounced from top to player1 ")
            elif ball_racket_distance(self.ball, self.player2) < ERR:
                print("ERROR: court.update: ball bounced from top to player2 ")
            return
        if self.ball.top >= self.top - self.app.adj_y * self.app.ft + ERR:
            self.ball.top = self.top - self.app.adj_y * self.app.ft
            self.ball.set_speed(self.ball.speed_x, min(-BALL_MIN_SPEED, -self.ball.speed_y))
            self.ball.set_spin(0)
            if ball_racket_distance(self.ball, self.player1) < ERR:
                print("ERROR: court.update: ball bounced from bottom to player1 ")
            elif ball_racket_distance(self.ball, self.player2) < ERR:
                print("ERROR: court.update: ball bounced from bottom to player2 ")
            return

        # mouse pointer hide/show
        if self.player1.free and self.player2.free:
            if self.mouse_pos != Window.mouse_pos:
                Window.show_cursor = True
                self.mouse_pos = Window.mouse_pos
                self.mouse_counter = int(0.3 * CYCLES_PER_SECOND)
            elif self.mouse_counter > 0:
                self.mouse_counter -= 1
            elif self.mouse_counter == 0:
                Window.show_cursor = False
                self.mouse_counter = -1

    def on_touch_down(self, touch):
        if self.player1.collide_point(touch.x, touch.y):
            self.player1.free = False
            self.player2.free = True
        elif self.player2.collide_point(touch.x, touch.y):
            self.player2.free = False
            self.player1.free = True

    def on_touch_move(self, touch):
        if not self.player1.free:
            self.player1.move_to_touch(touch)
        if not self.player2.free:
            self.player2.move_to_touch(touch)

    def on_touch_up(self, touch):
        self.player1.free = True
        self.player2.free = True
