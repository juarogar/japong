"""

"""
# env configures the kivy resources required for japong app
import japong.env

# https://github.com/kivy/kivy/issues/5434
# the following trick solves an issue with kivy getting confused with arguments passed to the console
# it could be considered to use os.environ['KIVY_NO_ARGS'] = 'T', but it has other effects
import sys
argv = sys.argv
sys.argv = sys.argv[:1]
from japong.app import JapongApp
sys.argv = argv


VERSION = {
    'major': 2,
    'minor': 0
}


def get_version_string():
    version = '{major}.{minor}'.format(**VERSION)
    return version


__version__ = get_version_string()

name = "japong"




