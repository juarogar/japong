"""

"""

# full-screen: int or string, one of 0, 1, ‘auto’
# If set to 1, a resolution of width times height pixels will be used.
# If set to auto, your current display’s resolution will be used instead. This is most likely what you want.
FULL_SCREEN = 'auto'

DECIMALS = 9 # used to round calculated values when they are stored
ERR = 1e-09 # used as threshold when comparing values

CYCLE = 20 # milliseconds
CYCLES_PER_SECOND = 1000. / CYCLE

# All units are in foot and foot/second (tennis court measures are defined in foot)
#  1 m = 3.28 foot  ->  1 foot = 0.3048m
#  1 km/h = 0.911 foot/s -> 1 foot / s = 1.097 km/h
BALL_DIAMETER = 2 # in foot... augmented, it would be difficult to play otherwise
BALL_MASS = 1 # real ball weights 58 grams
BALL_MIN_SPEED = 10.
BALL_MAX_SPEED = 200.
BALL_INIT_SPEED = 50.
BALL_MAX_SPIN = 15. # front hit ball speed rotation in degrees per second
BALL_MAX_NEAR_MISS_SPIN = 45 # max spin after near miss hit
BALL_FRICTION_COEFFICIENT = 0.06 # this slows down the ball
BALL_FRICTION_ACCELERATION = BALL_FRICTION_COEFFICIENT * 9.8 * 3.28
BALL_SPEED_DECAY = BALL_FRICTION_ACCELERATION / CYCLES_PER_SECOND # speed reduction per cycle

HIT_MAX_ANGLE = 15   # degrees
HIT_SWING_SPEED = 3 # ft/s

PLAYER_HEIGHT = 10 # in foot... includes extended arm and racket
PLAYER_WIDTH = 1.8 # in foot
PLAYER_MASS = 75 # kilograms
PLAYER_FRONT_STEP = 3 # distance jump per key stroke, in ft
PLAYER_BACK_STEP = 1
PLAYER_SIDE_STEP = 3.5
PLAYER_MAX_SPEED = 38. #  in ft/s
PLAYER_IMPULSE = 0.5 # in seconds
PLAYER_FRICTION_COEFFICIENT = 5.0 # friction coefficient, in opposite direction to movement.
PLAYER_FRICTION_ACCELERATION = PLAYER_FRICTION_COEFFICIENT * 9.8 * 3.28 # in ft / s2
PLAYER_SPEED_DECAY = PLAYER_FRICTION_ACCELERATION / CYCLES_PER_SECOND  # speed reduction per cycle
PLAYER_BRAKES = 100. # acceleration decrease per key stroke in ft/s2 (key opposite to movement)
PLAYER_MIN_ACCELERATION = 50. # acceleration increase per key stroke in ft/s2
PLAYER_MAX_ACCELERATION = 150. # max acceleration in ft/s2
PLAYER_ACCELERATION_DECAY = PLAYER_MIN_ACCELERATION / (PLAYER_IMPULSE * CYCLES_PER_SECOND) # acc. reduction per cycle

MOUSE_SPEED_ADJUSTMENT = 0.03

